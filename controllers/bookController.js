var Book = require('../models/book');
var Author = require('../models/author');
var Genre = require('../models/genre');
var BookInstance = require('../models/bookinstance');

var async = require('async');

exports.index = (req, res) => {
    async.parallel({
        book_count: function (callback) {
            Book.countDocuments({}, callback);
        },
        book_instance_count: function (callback) {
            BookInstance.countDocuments({}, callback);
        },
        author_count: function (callback) {
            Author.countDocuments({}, callback);
        },
        genre_count: function (callback) {
            Genre.countDocuments({}, callback);
        }
    }, function (err, results) {
        res.render('index', { title: "Welcome to LocLib", error: err, data: results });
    });
}
//display list of all books.
exports.book_list = (req, res, next) => {
    Book.find({}, 'title author')
        .populate('author')
        .exec((err, book_list) => {
            if (err) { return next(err); }
            res.render('book_list', { title: 'Book List', book_list: book_list });
        })
};

exports.book_detail = (req, res) => {
    res.send("N/A: book detail for " + req.params.id);
};

exports.book_create_get = (req, res) => {
    res.send("N/A");
}

exports.book_create_post = (req, res) => {
    res.send("N/A");
}

exports.book_update_get = (req, res) => {
    res.send("N/A");
}

exports.book_update_post = (req, res) => {
    res.send("N/A");
}

exports.book_delete_get = (req, res) => {
    res.send("N/A");
}

exports.book_delete_post = (req, res) => {
    res.send("N/A");
}