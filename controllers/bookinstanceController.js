var BookInstance = require('../models/bookinstance');

//display list of all bookinstances.
exports.bookinstance_list = (req, res, next) => {
    BookInstance.find()
        .populate('book')
        .exec(function (err, list_bookInstances) {
            if (err) { return next(err); }
            res.render('book_instance_list', { title: 'Book Instances', bookinstance_list: list_bookInstances });
        });
};

exports.bookinstance_detail = (req, res) => {
    res.send("N/A: bookinstance detail for " + req.params.id);
};

exports.bookinstance_create_get = (req, res) => {
    res.send("N/A");
}

exports.bookinstance_create_post = (req, res) => {
    res.send("N/A");
}

exports.bookinstance_update_get = (req, res) => {
    res.send("N/A");
}

exports.bookinstance_update_post = (req, res) => {
    res.send("N/A");
}

exports.bookinstance_delete_get = (req, res) => {
    res.send("N/A");
}

exports.bookinstance_delete_post = (req, res) => {
    res.send("N/A");
}