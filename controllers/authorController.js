var Author = require('../models/author');
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

//display list of all Authors.
exports.author_list = (req, res, next) => {
    Author.find()
        .sort([['family_name', 'ascending']])
        .exec(function (err, list_authors) {
            if (err) { return next(err); }
            res.render('author_list', { title: 'Author List', author_list: list_authors });
        })
};

exports.author_detail = (req, res) => {
    res.send("N/A: Author detail for " + req.params.id);
};

exports.author_create_get = (req, res, next) => {
    res.render('author_form', { title: 'Create Form' });
}

exports.author_create_post = (req, res) => [
    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified')
        .isAlphanumeric().withMessage('First name has non alpha-numeric characters'),
    body('family_name').isLength({ min: 1 }).trim().withMessage('Last name must be specified')
        .isAlphanumeric().withMessage('Family name has non alpha-numeric characters'),
    body('date_of_birth', 'Invalid Date of Birth').optional({ checkFalsy: true }).isISO8601(),
    body('date_of_death', 'Invalid Date of Death').optional({ checkFalsy: true }).isISO8601(),
    
    sanitizeBody('first_name').trim().escape(),
    sanitizeBody('family_name').trim().escape(),
    sanitizeBody('date_of_birth').toDate(),
    sanitizeBody('date_of_death').toDate(),

    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.render('author_form', { title: 'Create Author', author: req.body, errors: errors.array() });
            return;
        } else {
            var author = new Author({
                first_name: req.body.first_name,
                family_name: req.body.family_name,
                date_of_birth: req.body.date_of_birth,
                date_of_death: req.body.date_of_death
            });
            author.save(function (err) {
                if (err) { return next(err); }

                res.redirect(author.url);
            });

        }
    }
];

exports.author_update_get = (req, res) => {
    res.send("N/A");
}

exports.author_update_post = (req, res) => {
    res.send("N/A");
}

exports.author_delete_get = (req, res) => {
    res.send("N/A");
}

exports.author_delete_post = (req, res) => {
    res.send("N/A");
}